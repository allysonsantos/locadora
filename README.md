# Sistema de locadora 
Objetivo deste sistema é criar uma API REST utilizando NodeJS com banco dados MySQL.

## Tecnologias utilizadas

### NPM - v6.2.0
### Node - v10.9.0
### MySQL - v5.7.14

## Executar o sistema

### Etapa 1

```
git clone https://github.com/allysonsantos/locadora.git
cd locadora
npm install
```

### Etapa 2

- Precisa fazer a importação do dump.sql que está na pasta raiz deste projeto. 
- Após a importação do dump.sql, precisa editar o arquivo de configuração do sistema que se encontra no caminho: `config/default.json`. Você precisa apontar para seu banco de dados alterando os dados dentro de `mysql`:

```
{
    "server": {
        "port": 3050,
        "host": "0.0.0.0"
    },
    "mysql": {
        "host": "localhost",
        "user": "root",
        "password": "",
        "database": "locadora"
    },
    "jwt": {
        "secret": "locadora",
        "expiration_time": 3600
    },
    "version": 1
}
```

### Etapa 3

- Após garantir as etapas 1 e 2. Você precisa somente executar este comando dentro da pasta `locadora`, assim poderá utilizar API REST:

```
npm start
```

## Rotas da API

Está API utiliza JSON sobre HTTP. A sua utilização necessita de token (jwt) nas rotas do tipo GET. Caso não tenha o token, entenda-se que o usuário não está logado de fato. Lembrando, que todas as requisições precisam ser setados seu tipo de conteúdo na `headers`:

```
Content-Type: application/json
```

### POST /api/locadora/v1/login

Está rota serve para autenticar no sistema verificando seus dados no banco de dados e logo após gerando um token. Os dados de login devem ser passado atraves da `body`.

#### Request

```
{
  "login": "allyson.santos@hotmail.com",
  "senha": "allyson"
}
```

#### Response

```
{
    "success": "true",
    "data": {
        "nome": "Allyson Kremer",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1Mzg5NDU5NzQsImlhdCI6MTUzODk0MjM3NH0.XoyTZCt18Bt0FhY0dVy1-QxPaCm9kmWyIYUoMHd_i1k"
    }
}
```

### POST /api/locadora/v1/usuario

Está rota serve para criar o usuário. Os dados devem ser passados através da `body`.

#### Request

```
{
  "nome": "Allyson Kremer",
  "email": "allyson.santos@hotmail.com",
  "senha": "allyson"
}
```

#### Response

```
{
    "success": "true",
    "data": "Usuário cadastrado com sucesso!"
}
```

### GET /api/locadora/v1/listafilmes

Está rota serve para listar todos os filmes cadastrados. Está requisição necessita de `token` sendo passado na `headers`.

#### Request

```
token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1Mzg5NDU5NzQsImlhdCI6MTUzODk0MjM3NH0.XoyTZCt18Bt0FhY0dVy1-QxPaCm9kmWyIYUoMHd_i1k
```

#### Response

```
{
    "data": [
        {
            "idfilme": 1,
            "titulo": "O Poderoso Chefão",
            "diretor": "O Poderoso Chefão",
            "data_criacao": "2018-10-05T22:17:06.000Z"
        },
        {
            "idfilme": 2,
            "titulo": "O Mágico de Oz",
            "diretor": "O Mágico de Oz",
            "data_criacao": "2018-10-05T22:18:07.000Z"
        },
        {
            "idfilme": 3,
            "titulo": "Cidadão Kane",
            "diretor": "Cidadão Kane",
            "data_criacao": "2018-10-05T22:18:07.000Z"
        }
    ]
}
```

### GET /api/locadora/v1/titulofilme/:nome

Está rota serve para buscar o filme pelo título. Então precisa do nome do título, caso não funcionará a requisição. Está requisição necessita de `token` sendo passado na `headers`.

#### Request

```
token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1Mzg5NDU5NzQsImlhdCI6MTUzODk0MjM3NH0.XoyTZCt18Bt0FhY0dVy1-QxPaCm9kmWyIYUoMHd_i1k
```

#### Response

```
{
    "data": [
        {
            "idfilme": 1,
            "titulo": "O Poderoso Chefão",
            "diretor": "O Poderoso Chefão",
            "data_criacao": "2018-10-05T22:17:06.000Z"
        }
    ]
}
```

### GET /api/locadora/v1/locacaofilme/:nome

Está rota serve para buscar a situação do filme pelo título. Está situação (disponível ou indisponível) é a relação da locação do filme com o usuário. Precisa do nome do título, caso não funcionará a requisição. Está requisição necessita de `token` sendo passado na `headers`.

#### Request

```
token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1Mzg5NDU5NzQsImlhdCI6MTUzODk0MjM3NH0.XoyTZCt18Bt0FhY0dVy1-QxPaCm9kmWyIYUoMHd_i1k
```

#### Response

```
{
    "data": [
        {
            "titulo": "O Mágico de Oz",
            "situacao": "indisponível",
            "nome": "Allyson Oliveira Santos"
        }
    ]
}
```