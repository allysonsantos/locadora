"use strict";


const app = require("../src/app");
const http = require("http");
const express = require("express");
const config = require("config");
var path = require('path')
process.title = "node " + require(path.join(__dirname, '..', 'package')).name

app.set("ip", config.get("server.host"));
const port = config.get("server.port");
app.set("port", port);

const server = http.createServer(app);

server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

function onError(error) {
    if (error.syscall !== "listen") {
        throw error;
    }

    const bind = typeof port === "string" ?
        "Pipe " + port :
        "Port " + port;

    switch (error.code) {
        case "EACCES":
            console.error(bind + " requires elevated privileges");
            process.exit(1);
            break;
        case "EADDRINUSE":
            console.error(bind + " is already in use");
            process.exit(1);
            break;
        default:
            throw error;
    }
}

function onListening() {
    const addr = server.address();
    const bind = typeof addr === "string"
        ? "pipe " + addr
        : "port " + addr.port;
    console.log(`Worker ${process.pid} started - Connectou ${bind}`);
}
