-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 07-Out-2018 às 17:10
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `locadora`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `filmes`
--

CREATE TABLE `filmes` (
  `idfilme` int(11) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `diretor` varchar(45) NOT NULL,
  `data_criacao` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `filmes`
--

INSERT INTO `filmes` (`idfilme`, `titulo`, `diretor`, `data_criacao`) VALUES
(1, 'O Poderoso Chefão', 'O Poderoso Chefão', '2018-10-06 01:17:06'),
(2, 'O Mágico de Oz', 'O Mágico de Oz', '2018-10-06 01:18:07'),
(3, 'Cidadão Kane', 'Cidadão Kane', '2018-10-06 01:18:07'),
(4, 'Um Sonho de Liberdade', 'Um Sonho de Liberdade', '2018-10-06 01:18:45'),
(5, 'Pulp Fiction', 'Pulp Fiction', '2018-10-06 01:18:45');

-- --------------------------------------------------------

--
-- Estrutura da tabela `locacoes`
--

CREATE TABLE `locacoes` (
  `idusuario` int(11) NOT NULL,
  `idfilme` int(11) NOT NULL,
  `idsitacao` int(11) NOT NULL,
  `data_criacao` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `locacoes`
--

INSERT INTO `locacoes` (`idusuario`, `idfilme`, `idsitacao`, `data_criacao`) VALUES
(1, 1, 1, '2018-10-07 18:29:01'),
(1, 2, 2, '2018-10-07 18:29:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sitacoes`
--

CREATE TABLE `sitacoes` (
  `idsitacao` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `data_criacao` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `sitacoes`
--

INSERT INTO `sitacoes` (`idsitacao`, `nome`, `data_criacao`) VALUES
(1, 'disponível', '2018-10-07 18:23:49'),
(2, 'indisponível', '2018-10-07 18:23:49');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `email` varchar(45) NOT NULL,
  `senha` varchar(128) NOT NULL,
  `data_criacao` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`idusuario`, `nome`, `email`, `senha`, `data_criacao`) VALUES
(1, 'Allyson Santos', 'allyson.in@gmail.com', '425632dab4617d15a1b9f411499bd09f88ed3dd8bc40603aeb9ed5e758a0d91c5fbdf12a66dd586d331bedd775ddabf95e0744ff22a1264a99e8670e8f0ec5bd', '2018-10-07 19:55:10'),
(3, 'Allyson Kremer', 'allyson.santos@hotmail.com', '425632dab4617d15a1b9f411499bd09f88ed3dd8bc40603aeb9ed5e758a0d91c5fbdf12a66dd586d331bedd775ddabf95e0744ff22a1264a99e8670e8f0ec5bd', '2018-10-07 19:50:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `filmes`
--
ALTER TABLE `filmes`
  ADD PRIMARY KEY (`idfilme`);

--
-- Indexes for table `locacoes`
--
ALTER TABLE `locacoes`
  ADD PRIMARY KEY (`idusuario`,`idfilme`,`idsitacao`),
  ADD KEY `fk_usuarios_has_filmes_filmes1_idx` (`idfilme`),
  ADD KEY `fk_usuarios_has_filmes_usuarios_idx` (`idusuario`),
  ADD KEY `fk_locacoes_sitacoes1_idx` (`idsitacao`);

--
-- Indexes for table `sitacoes`
--
ALTER TABLE `sitacoes`
  ADD PRIMARY KEY (`idsitacao`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `filmes`
--
ALTER TABLE `filmes`
  MODIFY `idfilme` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sitacoes`
--
ALTER TABLE `sitacoes`
  MODIFY `idsitacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `locacoes`
--
ALTER TABLE `locacoes`
  ADD CONSTRAINT `fk_locacoes_sitacoes1` FOREIGN KEY (`idsitacao`) REFERENCES `sitacoes` (`idsitacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_has_filmes_filmes1` FOREIGN KEY (`idfilme`) REFERENCES `filmes` (`idfilme`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_has_filmes_usuarios` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
