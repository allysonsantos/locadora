"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const helmet = require("helmet");
const xssFilter = require("x-xss-protection");
const compression = require("compression");
const morgan = require("morgan");
const config = require("config");

const app = express();
const router = express.Router();
const baseUrl = `/api/locadora/v${config.get("version")}`;

app.use(helmet())
app.use(helmet.xssFilter())
app.use(xssFilter())
app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(compression());

// Carrega as Rotas
const locadoraRoutes = require("./routes/locadora-route.js");

// Carrega Middleware
const genericMiddleware = require("./middlewares/generic-middleware");

app.use(`${baseUrl}`, genericMiddleware, locadoraRoutes);

module.exports = app;

