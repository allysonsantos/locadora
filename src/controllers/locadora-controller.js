"use strict";

const service = require("../services/locadora-service");

const controller = {};

controller.login = (request, response) => {
    try {
        service.login(request.body.login, request.body.senha, function (err, results) {
            if (err) {
                return response.status(401).json({ message: "Não foi possível autenticar o usuário" }).end();
            }
            return response.status(200).json(results).end();
        });
    } catch (error) {
        return response.status(500).json({ status: "false", message: error }).end();
    }
};

controller.criarUsuario = (request, response) => {
    try {
        service.criarUsuario(request.body.nome, request.body.email, request.body.senha, function (err, results) {
            if (err) {
                return response.status(401).json({ message: "Não foi possível criar a conta do usuário" }).end();
            }
            return response.status(200).json(results).end();
        });
    } catch (error) {
        return response.status(500).json({ status: "false", message: error }).end();
    }
};

controller.listaFilmes = (request, response) => {
    try {
        service.listaFilmes(function (err, results) {
            if (err) {
                return response.status(401).json({ message: "Não foi possível buscar a lista dos filmes" }).end();
            }
            return response.status(200).json(results).end();
        });
    } catch (error) {
        return response.status(500).json({ status: "false", message: error }).end();
    }
};

controller.tituloFilme = (request, response) => {
    try {
        service.tituloFilme(request.params.nome, function (err, results) {
            if (err) {
                return response.status(401).json({ message: "Não foi possível buscar o filme pelo título" }).end();
            }
            return response.status(200).json(results).end();
        });
    } catch (error) {
        return response.status(500).json({ status: "false", message: error }).end();
    }
};

controller.locacaoFilme = (request, response) => {
    try {
        service.locacaoFilme(request.params.nome, function (err, results) {
            if (err) {
                return response.status(401).json({ message: "Não foi possível verificar a situação do filme" }).end();
            }
            return response.status(200).json(results).end();
        });
    } catch (error) {
        return response.status(500).json({ status: "false", message: error }).end();
    }
};

module.exports = controller;