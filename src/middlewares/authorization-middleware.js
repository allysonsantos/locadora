"use strict";

const jwt = require('jsonwebtoken');
const config = require("config");

const authorizationMiddleWare = {};

authorizationMiddleWare.authorization = (request, response, next) => {

    try {
        if (request.headers.token) {
            let secret = config.get("jwt.secret");
            let decoded = jwt.verify(request.headers.token, secret);
            return next();
        }
    }
    catch {
        return response.status(400).json({ status: "false", message: "Você não está autorizado!" }).end();
    }
}

module.exports = authorizationMiddleWare;

