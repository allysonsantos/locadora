"use strict";

const locadoraValidator = require("../validators/locadora-validator");

const locadoraMiddleWare = {};

locadoraMiddleWare.validarLogin = (request, response, next) => {

    let validator = new locadoraValidator();

    let result = validator.validarLogin(request.body);

    if(result.status){
        return response.status(result.status).json({
            message: result.message,
            data: result.data
        });
    }

    return next();
}

locadoraMiddleWare.validarUsuario = (request, response, next) => {

    let validator = new locadoraValidator();

    let result = validator.validarUsuario(request.body);

    if(result.status){
        return response.status(result.status).json({
            message: result.message,
            data: result.data
        });
    }

    return next();
}

module.exports = locadoraMiddleWare;
