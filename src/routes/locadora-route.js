"use strict";

const express = require("express");
const router = express.Router();
const authorizationMiddleware = require("../middlewares/authorization-middleware");

const locadoraController = require("../controllers/locadora-controller");
const locadoraMiddleware = require("../middlewares/locadora-middleware");

// Rota para fazer o login
router.post("/login", locadoraMiddleware.validarLogin, locadoraController.login);
// Rota para criar usuário
router.post("/usuario", locadoraMiddleware.validarUsuario, locadoraController.criarUsuario);
// Rota para lista de filmes
router.get("/listafilmes", authorizationMiddleware.authorization, locadoraController.listaFilmes);
// Rota para buscar o filme pelo título
router.get("/titulofilme/:nome", authorizationMiddleware.authorization, locadoraController.tituloFilme);
// Rota para verificar a situação da locação de algum filme
router.get("/locacaofilme/:nome", authorizationMiddleware.authorization, locadoraController.locacaoFilme)

module.exports = router
