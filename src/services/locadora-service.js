"use strict";

const Mysql = require("../services/mysql-service");
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const config = require("config");

const service = {};

service.login = (login, senha, cb) => {

    try {

        let locadora = new Mysql();
        let connect = locadora.connect();

        if (connect) {
            let sha512 = crypto.createHash('sha512').update(senha, 'utf8').digest('hex');
            locadora.login(login, sha512, function (err, results) {
                if (err) {
                    cb(undefined, err);
                }
                if (results && Object.keys(results).length) {
                    // Gerar o token depois de verificado o login e senha no banco de dados
                    let secret = config.get("jwt.secret");
                    let expiration_time = config.get("jwt.expiration_time");
                    let token = jwt.sign({
                        exp: Math.floor(Date.now() / 1000) + (expiration_time),
                    }, secret);

                    let data = { "nome": results[0].nome, "token": token };
                    let login = { "success": "true", "data": data };
                    cb(undefined, login);
                } else {
                    let login = { "mensagem": "Login ou senha está incorreto!" };
                    cb(undefined, login);
                }
            });
        }

    } catch (error) {
        console.log("Customer ticket error :: ", error);
        throw error;
    }
}

service.criarUsuario = (nome, email, senha, cb) => {

    try {

        let locadora = new Mysql();
        let connect = locadora.connect();

        if (connect) {
            locadora.consultarUsuario(email, function (err, results) {
                if (err) {
                    cb(undefined, err);
                }
                if (results && Object.keys(results).length) {
                    let usuario = { "mensagem": "Já existe um cadastro com este email: " + email };
                    cb(undefined, usuario);
                } else {
                    let sha512 = crypto.createHash('sha512').update(senha, 'utf8').digest('hex');
                    locadora.criarUsuario(nome, email, sha512, function (err, results) {
                        if (err) {
                            cb(undefined, err);
                        }
                        if (results && Object.keys(results).length) {
                            let data = "Usuário cadastrado com sucesso!";
                            let usuario = { "success": "true", "data": data };
                            cb(undefined, usuario);
                        }
                    });
                }
            });
        }

    } catch (error) {
        console.log("Customer ticket error :: ", error);
        throw error;
    }
}

service.listaFilmes = (cb) => {

    try {

        let locadora = new Mysql();
        let connect = locadora.connect();

        if (connect) {
            locadora.listaFilmes(function (err, results) {
                if (err) {
                    cb(undefined, err);
                }
                if (results && Object.keys(results).length) {
                    let lista_filmes = { "data": results };
                    cb(undefined, lista_filmes);
                } else {
                    let lista_filmes = { "data": [] };
                    cb(undefined, lista_filmes);
                }
            });
        }

    } catch (error) {
        console.log("Customer ticket error :: ", error);
        throw error;
    }
}

service.tituloFilme = (titulo, cb) => {

    try {

        let locadora = new Mysql();
        let connect = locadora.connect();

        if (connect) {
            let like_titulo = `%` + titulo + `%`;
            locadora.tituloFilme(like_titulo, function (err, results) {
                if (err) {
                    cb(undefined, err);
                }
                if (results && Object.keys(results).length) {
                    let filme = { "data": results };
                    cb(undefined, filme);
                } else {
                    let filme = { "data": [] };
                    cb(undefined, filme);
                }
            });
        }

    } catch (error) {
        console.log("Customer ticket error :: ", error);
        throw error;
    }
}

service.locacaoFilme = (titulo, cb) => {

    try {

        let locadora = new Mysql();
        let connect = locadora.connect();

        if (connect) {
            let like_titulo = `%` + titulo + `%`;
            locadora.locacaoFilme(like_titulo, function (err, results) {
                if (err) {
                    cb(undefined, err);
                }
                if (results && Object.keys(results).length) {
                    let filme = { "data": results };
                    cb(undefined, filme);
                } else {
                    let filme = { "data": [] };
                    cb(undefined, filme);
                }
            });
        }

    } catch (error) {
        console.log("Customer ticket error :: ", error);
        throw error;
    }
}

module.exports = service;