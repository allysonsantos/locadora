"use strict";

class Mysql {

    // Construtor para setar as variáveis de acesso ao banco de dados mysql
    constructor() {

        const config = require("config");
        const mysql = require('mysql');

        let host = config.get("mysql.host");
        let user = config.get("mysql.user");
        let pass = config.get("mysql.password");
        let db = config.get("mysql.database");

        this.db = mysql.createConnection({
            host: host,
            user: user,
            password: pass,
            database: db
        });
    }

    // Função para conectar no banco de dados mysql
    connect() {
        try {
            const result = this.db.connect((err) => {
                if (err) {
                    console.log("MySQL error :: ", err);
                    return false;
                }
            });
            return true;
        } catch (error) {
            console.log("MySQL error :: ", error);
            throw error;
        }
    }

    // Função para verificar o login e senha na tabela usuarios
    login(login, senha, cb) {
        this.db.query("SELECT * FROM usuarios WHERE email LIKE ? AND senha LIKE ?", [login, senha],
            function (err, results) {
                if (err) {
                    console.log("ERROR :: ", err);
                    cb(undefined, err);
                }
                cb(undefined, results);
            });
    }

    // Função para consultar usuario pelo email
    consultarUsuario(email, cb) {
        this.db.query("SELECT * FROM usuarios WHERE email LIKE ?", [email],
            function (err, results) {
                if (err) {
                    console.log("ERROR :: ", err);
                    cb(undefined, err);
                }
                cb(undefined, results);
            });
    }

    // Função para criar usuário
    criarUsuario(nome, email, senha, cb) {
        this.db.query("INSERT INTO usuarios (idusuario, nome, email, senha, data_criacao) VALUES (NULL, ?, ?, ?, NOW())", [nome, email, senha],
            function (err, results) {
                if (err) {
                    console.log("ERROR :: ", err);
                    cb(undefined, err);
                }
                cb(undefined, results);
            });
    }


    // Função para buscar os filmes
    listaFilmes(cb) {
        this.db.query("SELECT * FROM filmes",
            function (err, results) {
                if (err) {
                    console.log("ERROR :: ", err);
                    cb(undefined, err);
                }
                cb(undefined, results);
            });
    }

    // Função para buscar o filme pelo titulo
    tituloFilme(titulo, cb) {
        this.db.query("SELECT * FROM filmes WHERE titulo LIKE ?", [titulo],
            function (err, results) {
                if (err) {
                    console.log("ERROR :: ", err);
                    cb(undefined, err);
                }
                cb(undefined, results);
            });
    }

    // Função para buscar a situação do filme pelo titulo
    locacaoFilme(titulo, cb) {
        this.db.query("SELECT F.titulo, S.nome AS situacao, U.nome FROM filmes AS F INNER JOIN locacoes AS L ON F.idfilme = L.idfilme INNER JOIN sitacoes AS S ON L.idsitacao = S.idsitacao INNER JOIN usuarios AS U ON L.idusuario = U.idusuario WHERE titulo LIKE ?", [titulo],
            function (err, results) {
                if (err) {
                    console.log("ERROR :: ", err);
                    cb(undefined, err);
                }
                cb(undefined, results);
            });
    }
}

module.exports = Mysql;