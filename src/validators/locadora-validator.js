"use strict";

const GenericValidator = require("./generic-validator");

class LocadoraValidator extends GenericValidator {
    constructor() {
        super();
    }

    validarLogin(body) {

        this.isRequired(body.login, "Precisa preencher o campo login");
        this.isRequired(body.senha, "Precisa preencher o campo senha");

        // Se os dados forem inválidos
        if (!this.isValid()) {
            return {
                status: 400,
                message: "Dados incorretos, verifique os campos preenchidos:",
                data: this.errors
            };
        }
        return true;
    }

    validarUsuario(body) {

        this.isRequired(body.nome, "Precisa preencher o campo nome");
        this.isRequired(body.email, "Precisa preencher o campo email");
        this.isRequired(body.senha, "Precisa preencher o campo senha");

        // Se os dados forem inválidos
        if (!this.isValid()) {
            return {
                status: 400,
                message: "Dados incorretos, verifique os campos preenchidos:",
                data: this.errors
            };
        }
        return true;
    }
}

module.exports = LocadoraValidator;
